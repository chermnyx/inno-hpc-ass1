__kernel void OclRegMult(__global const double *A, __global const double *x, __global double *y) {
    // Get the index of the current element to be processed
    int i = get_global_id(0);
    // Do the operation
    y[i] = 0;
    for (int j = 0; j < A.N; ++j) {
        y[i] += A[A.N * i + j] * x[j];
    }
}
