#pragma once

#include <iostream>
#include <iomanip>
#include "mtx.cpp"
#include "helpers.cpp"

void genMatrix(int N, crsMatrix &A, regMatrix &B) {
    auto rowN = N / 2;
    auto NZ = rowN * N;

    AllocateCRSMatrix(N, NZ, A);
    AllocateRegMatrix(N, B);

    GenerateRegularCRS(N, rowN, A);
//    printM(A.NZ, A.Value);
//    std::cout << std::endl;
//    printM(A.NZ, A.Col);
//    std::cout << std::endl;
//    printM(A.N + 1, A.RowIndex);
//    std::cout << std::endl;
    CRStoReg(A, &B);

}

void genVec(int N, double **x) {
    AllocateVector(N, x);
    for (auto i = 0; i < N; ++i) {
        (*x)[i] = next() * MAX_VAL;
    }
}

void task12() {
    auto v = std::vector<int>{10, 100, 1000};

    for (auto N: v) {
        crsMatrix A;
        regMatrix B;
        double *x;
        genMatrix(N, A, B);
        genVec(N, &x);

        double *y_r;
        AllocateVector(A.N, &y_r);
        double *y_c;
        AllocateVector(A.N, &y_c);

        double t;
        double diff;

        SeqRegMult(B, x, y_r, t);
        std::cout << "### N = " << N << std::endl;
        std::cout << "SeqRegMult time=" << t;
        std::cout << ", result=";
        printM(N, y_r);
        std::cout << std::endl;

        SeqCRSMult(A, x, y_c, t);
        std::cout << "SeqCRSMult time=" << t
                  << ", " << ((CompareVectors(y_c, y_r, N, diff) == 0) ? "correct" : "incorrect");
        std::cout << ", result=";
        printM(N, y_c);
        std::cout << std::endl;

        FreeCRSMatrix(A);
        FreeRegMatrix(B);
        FreeVector(&x);
        FreeVector(&y_r);
        FreeVector(&y_c);
    }
}

void task14(int tnum) {
    std::vector<int> NNN;
    std::vector<double> serial;
    std::vector<double> omp;

    // USING N <= 1500 because larger N takes too long to compute serially
    for (auto N = 500; N <= 1500; N += 500) {
        crsMatrix A;
        regMatrix B;
        double *x;
        genMatrix(N, A, B);
        genVec(N, &x);

        double *y_r;
        AllocateVector(A.N, &y_r);
        double *y_c;
        AllocateVector(A.N, &y_c);

        double t;
        double diff;

        SeqRegMult(B, x, y_r, t);
        std::cout << "### N=" << N
                  << ", tnum=" << tnum
                  << std::endl;
        std::cout << "SeqRegMult time=" << t;
        std::cout << ", result=";
        printM(N, y_r);
        std::cout << std::endl;
        serial.push_back(t);

        OmpRegMult(B, x, y_c, tnum, t);
        std::cout << "OmpRegMult time=" << t
                  << ", " << ((CompareVectors(y_c, y_r, N, diff) == 0) ? "correct" : "incorrect");
        std::cout << ", result=";
        printM(N, y_c);
        std::cout << std::endl;
        omp.push_back(t);

        NNN.push_back(N);
    }

    auto plot = get_gnuplot();
    *plot << "set key\n";
    *plot << "set title 'omp task14 tnum=" << tnum << "'\n";
    *plot << "plot '-' using 1:2 title 'serial' with lines,\\\n";
    *plot << "'-' using 1:2 title 'omp' with lines\n";

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << serial[i] << "\n";
    }
    *plot << "e" << std::endl;

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << omp[i] << "\n";
    }
    *plot << "e" << std::endl;
}

void task16(int tnum) {
    std::vector<int> NNN;
    std::vector<double> serial;
    std::vector<double> omp;

    // USING N <= 1500 because larger N takes too long to compute serially
    for (auto N = 500; N <= 1500; N += 500) {
        crsMatrix A;
        regMatrix B;
        double *x;
        genMatrix(N, A, B);
        genVec(N, &x);

        double *y_r;
        AllocateVector(A.N, &y_r);
        double *y_c;
        AllocateVector(A.N, &y_c);

        double t;
        double diff;

        SeqCRSMult(A, x, y_r, t);
        std::cout << "### N=" << N
                  << ", tnum=" << tnum
                  << std::endl;
        std::cout << "SeqCRSMult time=" << t;
        std::cout << ", result=";
        printM(N, y_r);
        std::cout << std::endl;
        serial.push_back(t);

        OmpCRSMult(A, x, y_c, tnum, t);
        std::cout << "OmpCRSMult time=" << t
                  << ", " << ((CompareVectors(y_c, y_r, N, diff) == 0) ? "correct" : "incorrect");
        std::cout << ", result=";
        printM(N, y_c);
        std::cout << std::endl;
        omp.push_back(t);

        NNN.push_back(N);
    }

    auto plot = get_gnuplot();
    *plot << "set key\n";
    *plot << "set title 'omp task16 tnum=" << tnum << "'\n";
    *plot << "plot '-' using 1:2 title 'serial' with lines,\\\n";
    *plot << "'-' using 1:2 title 'omp' with lines\n";

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << serial[i] << "\n";
    }
    *plot << "e" << std::endl;

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << omp[i] << "\n";
    }
    *plot << "e" << std::endl;
}

void task19() {
    auto v = std::vector<int>{3, 10, 100, 1000};

    for (auto N: v) {
        crsMatrix A;
        regMatrix AA;
        crsMatrix B;
        regMatrix BB;
        genMatrix(N, A, AA);
        genMatrix(N, B, BB);

        crsMatrix C;
        regMatrix CC;
        regMatrix C1;
        AllocateRegMatrix(A.N, CC);
        AllocateRegMatrix(A.N, C1);

        printM(A);
        std::cout << "\n\n";
        printM(B);
        std::cout << "\n\n";

        double t;

        SeqRegMult(AA, BB, C1, t);
        std::cout << "### N=" << N << std::endl;
        std::cout << "SeqRegMult time=" << t;
        std::cout << ", result=\n";
        printM(C1);
        std::cout << std::endl;

        SeqCRSMult(A, B, C, t);
        CRStoReg(C, &CC);
        std::cout << "SeqCRSMult time=" << t
                  << ", " << ((CompareMatrices(CC, C1) == 0) ? "correct" : "incorrect");
        std::cout << ", result=\n";
        printM(CC);
        std::cout << std::endl;

        FreeCRSMatrix(A);
        FreeCRSMatrix(B);
        FreeCRSMatrix(C);
        FreeRegMatrix(AA);
        FreeRegMatrix(BB);
        FreeRegMatrix(CC);
        FreeRegMatrix(C1);
        break;
    }
}

void task21(int tnum) {
    std::vector<int> NNN;
    std::vector<double> serial;
    std::vector<double> omp;

    // USING N <= 1500 because larger N takes too long to compute serially
    for (auto N = 500; N <= 1500; N += 500) {
        crsMatrix A;
        regMatrix AA;
        crsMatrix B;
        regMatrix BB;
        genMatrix(N, A, AA);
        genMatrix(N, B, BB);

        regMatrix CC;
        regMatrix C1;
        AllocateRegMatrix(A.N, CC);
        AllocateRegMatrix(A.N, C1);

        double t;

        SeqRegMult(AA, BB, C1, t);
        std::cout << "### N=" << N << std::endl;
        std::cout << "SeqRegMult time=" << t;
        std::cout << ", result=\n";
//        printM(C1);
        std::cout << std::endl;
        serial.push_back(t);

        OmpRegMult(AA, BB, CC, tnum, t);
        std::cout << "OmpRegMult time=" << t
                  << ", " << ((CompareMatrices(CC, C1) == 0) ? "correct" : "incorrect");
        std::cout << ", result=\n";
//        printM(CC);
        std::cout << std::endl;
        omp.push_back(t);

        FreeCRSMatrix(A);
        FreeCRSMatrix(B);
        FreeRegMatrix(AA);
        FreeRegMatrix(BB);
        FreeRegMatrix(CC);
        FreeRegMatrix(C1);

        NNN.push_back(N);
    }

    auto plot = get_gnuplot();
    *plot << "set key\n";
    *plot << "set title 'omp task21 tnum=" << tnum << "'\n";
    *plot << "plot '-' using 1:2 title 'serial' with lines,\\\n";
    *plot << "'-' using 1:2 title 'omp' with lines\n";

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << serial[i] << "\n";
    }
    *plot << "e" << std::endl;

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << omp[i] << "\n";
    }
    *plot << "e" << std::endl;
}

void task23(int tnum) {
    std::vector<int> NNN;
    std::vector<double> serial;
    std::vector<double> omp;

    // USING N <= 1500 because larger N takes too long to compute serially
    for (auto N = 500; N <= 1500; N += 500) {
        crsMatrix A;
        regMatrix AA;
        crsMatrix B;
        regMatrix BB;
        genMatrix(N, A, AA);
        genMatrix(N, B, BB);

        crsMatrix C1;
        crsMatrix C2;
//        AllocateCRSMatrix(A.N, A.NZ, C1);
//        AllocateCRSMatrix(A.N, A.NZ, C2);

        regMatrix CC1, CC2;
        AllocateRegMatrix(A.N, CC1);
        AllocateRegMatrix(A.N, CC2);

        double t;

        SeqCRSMult(A, B, C1, t);
        CRStoReg(C1, &CC1);
        std::cout << "### N=" << N << std::endl;
        std::cout << "SeqRegMult time=" << t;
        std::cout << ", result=\n";
//        printM(C1);
        std::cout << std::endl;
        serial.push_back(t);

        OmpCRSMult(A, B, C2, tnum, t);
        CRStoReg(C1, &CC2);
        std::cout << "OmpRegMult time=" << t
                  << ", " << ((CompareMatrices(CC1, CC2) == 0) ? "correct" : "incorrect");
        std::cout << ", result=\n";
//        printM(CC);
        std::cout << std::endl;
        omp.push_back(t);

        FreeCRSMatrix(A);
        FreeCRSMatrix(B);
        FreeRegMatrix(AA);
        FreeRegMatrix(BB);
        FreeRegMatrix(CC1);
        FreeRegMatrix(CC2);
        FreeCRSMatrix(C1);
        FreeCRSMatrix(C2);

        NNN.push_back(N);
    }

    auto plot = get_gnuplot();
    *plot << "set key\n";
    *plot << "set title 'omp task23 tnum=" << tnum << "'\n";
    *plot << "plot '-' using 1:2 title 'serial' with lines,\\\n";
    *plot << "'-' using 1:2 title 'omp' with lines\n";

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << serial[i] << "\n";
    }
    *plot << "e" << std::endl;

    for (auto i = 0; i < NNN.size(); ++i) {
        *plot << NNN[i] << " " << omp[i] << "\n";
    }
    *plot << "e" << std::endl;
}

int main() {
    std::cout << "Dmitrii Chermnykh\n";
    std::cout << "Programming assignment #01" << std::endl;

    std::cout << "CPU INFO:\n";
    auto cpuinfo = read_path("/proc/cpuinfo");
    std::cout << cpuinfo;
    std::cout << "Memory info:\n" << read_path("/proc/meminfo");
    getOclDevice(true);

    srand(3654);

    task12();

    task14(2);
    task14(4);
    task14(8);

    task16(2);
    task16(4);
    task16(8);

    task19();
    task21(2);
    task21(4);
    task21(8);

    task23(2);
    task23(4);
    task23(8);

    return 0;
}
