#pragma once

#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 200

#include <ext/stdio_filebuf.h>
#include <CL/opencl.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <sstream>
#include <fstream>

#define platform_ind 0
#define GNUPLOT_COMMAND "gnuplot -persist"

std::string read_path(const std::string &path) {
    auto s = std::ostringstream();
    std::ifstream input_file(path);
    if (!input_file.is_open()) {
        throw std::runtime_error("Unable to open " + path);
    }
    s << input_file.rdbuf();
    return s.str();
}

cl::Device getOclDevice(bool verbose) {
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    auto p = platforms[platform_ind];

    cl::Platform newP = cl::Platform::setDefault(p);
    if (newP != p) {
        throw std::runtime_error("Error setting default platform.");
    }

    cl::Device d = cl::Device::getDefault();

    if (verbose) {
        auto platver = p.getInfo<CL_PLATFORM_VERSION>();
        auto device = d.getInfo<CL_DEVICE_NAME>();
        auto clk = d.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>();
        auto cores = d.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
        auto gram = d.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();

        std::cout << "Using platform\n"
                  << "  name " << device << ",\n"
                  << "  OCL version " << platver << ",\n"
                  << "  clkRate " << clk << ",\n"
                  << "  compute units " << cores << ",\n"
                  << "  gram " << gram / 1024 / 1024 << " MiB\n"
                  << std::endl;
    }

    return d;
}


std::ostream* get_gnuplot() {
    FILE *gnuplotfd = popen(GNUPLOT_COMMAND, "w");
    if (gnuplotfd == nullptr)
        throw std::runtime_error("UNABLE TO START GNUPLOT");
    auto filebuf = new __gnu_cxx::stdio_filebuf<char>(gnuplotfd, std::ios::out);
    auto plot = new std::ostream(filebuf);
    return plot;
}

template<typename A, typename B>
void plotPoints(std::vector<A> x, std::vector<B> y, char* title) {
    FILE *gnuplotfd = popen(GNUPLOT_COMMAND, "w");
    if (gnuplotfd == nullptr)
        throw std::runtime_error("UNABLE TO START GNUPLOT");
    __gnu_cxx::stdio_filebuf<char> filebuf(gnuplotfd, std::ios::out);
    std::ostream plot(&filebuf);

    plot << "'-' using 1:2 title 'points' with lines\n";
    for (auto i = 0; i < x.size(); ++i) {
        plot << x[i] << " " << y[i] << "\n";
    }
    plot << "e" << std::endl;
}