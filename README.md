# HPC Programming assignment #01

Dmitrii Chermnykh <d.chermnykh@innopolis.university>

## Source code

Note: some matrix sizes were reduced from the given ones because it took too much time to compute products for big sizes.

<https://gitlab.com/chermnyx/inno-hpc-ass1>

## Output

### Text output

[Text](./output.txt)

### Graphs

![Gnuplot](./Screenshot_20220220_150801.png)

## Conclusion

For some cases OpenMP is not efficient because it has big overhead. 
For CRS form with size increase omp multiplication becomes slower because it spends too much time on threads
synchronization due to `#pragma omp ordered`
