__kernel void OclCRSMult(__global const double *Value,
                         __global const int *Col,
                         __global const int *RowIndex,
                         __global const double *x,
                         __global double *y) {
    // Get the index of the current element to be processed
    int i = get_global_id(0);

    double sum = 0;
    int j1 = RowIndex[i];
    int j2 = RowIndex[i + 1];
    for (int j = j1; j < j2; j++)
        sum = sum + Value[j] * x[Col[j]];
    y[i] = sum;
}
