#pragma once

#include <cstring>
#include <ctime>
#include <omp.h>

#include <CL/opencl.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cstring>
#include "helpers.cpp"

#define MAX_VAL 100
#define EPS 0.0001

typedef struct crsMatrix {
// Matrix size (N x N)
    int N;
// Number of non-zero elements
    int NZ;
// Array of values (size NZ)
    double *Value;
// Array of column numbers (size NZ)
    int *Col;
// Array of row indexes (size N + 1)
    int *RowIndex;
} crsMatrix;

typedef struct regMatrix {
    int N; // size
    double *Value;  // values
} regMatrix;

void printM(regMatrix A) {
    for (auto i = 0; i < A.N; ++i) {
        if (i != 0) std::cout << "," << std::endl;

        if (i == 0) std::cout << "[";
        else std::cout << " ";

        for (auto j = 0; j < A.N; ++j) {
            if (j != 0) std::cout << ", ";
            std::cout << std::setw(8);
            std::cout << A.Value[i * A.N + j];
        }
    }
    std::cout << "]\n";
}

void AllocateRegMatrix(int N, regMatrix &mtx);

void FreeRegMatrix(regMatrix &mtx);

int CRStoReg(crsMatrix iM, regMatrix *oM);

void printM(crsMatrix A) {
    regMatrix M;
    AllocateRegMatrix(A.N, M);
    CRStoReg(A, &M);
    printM(M);
    FreeRegMatrix(M);
}

template<typename T>
void printM(int N, T *v) {
    for (auto i = 0; i < N; ++i) {
        if (i != 0) std::cout << " ";
        std::cout << v[i];
    }
}


void AllocateCRSMatrix(int N, int NZ, crsMatrix &mtx) {
    mtx.Value = static_cast<double *>(calloc(NZ, sizeof(double)));
    mtx.Col = static_cast<int *>(calloc(NZ, sizeof(int)));
    mtx.RowIndex = static_cast<int *>(calloc(N + 1, sizeof(int)));
    mtx.N = N;
    mtx.NZ = NZ;
}

void AllocateRegMatrix(int N, regMatrix &mtx) {
    mtx.Value = static_cast<double *>(calloc(N * N, sizeof(double)));
    mtx.N = N;
}

void AllocateVector(int N, double **vec) {
    *vec = static_cast<double *>(calloc(N, sizeof(double)));
}

void FreeRegMatrix(regMatrix &mtx) {
    free(mtx.Value);
}

void FreeCRSMatrix(crsMatrix &mtx) {
    free(mtx.Value);
    free(mtx.Col);
    free(mtx.RowIndex);
}

void FreeVector(double **vec) {
    free(*vec);
}

void CopyRegMatrix(regMatrix im, regMatrix &om) {
    memcpy(om.Value, im.Value, im.N);
}

void CopyCRSMatrix(crsMatrix im, crsMatrix &om) {
    memcpy(om.Value, im.Value, im.NZ);
    memcpy(om.Col, im.Col, im.NZ);
    memcpy(om.RowIndex, im.RowIndex, im.N + 1);
}

void CopyVector(int N, double **iv, double **ov) {
//    AllocateVector(N, ov);
    memcpy(*ov, *iv, N);
}

double next() {
    return ((double) rand() / (double) RAND_MAX);
}

void GenerateRegularCRS(int N, int cntInRow, crsMatrix &mtx) {
    int i, j, k, f, tmp, notNull, c;

    notNull = cntInRow * N;
    AllocateCRSMatrix(N, notNull, mtx);

    for (i = 0; i < N; i++) {
        for (j = 0; j < cntInRow; j++) {
            do {
                mtx.Col[i * cntInRow + j] = rand() % N;
                f = 0;
                for (k = 0; k < j; k++)
                    if (mtx.Col[i * cntInRow + j] == mtx.Col[i * cntInRow + k])
                        f = 1;
            } while (f == 1);
        }
        for (j = 0; j < cntInRow - 1; j++)
            for (k = 0; k < cntInRow - 1; k++)
                if (mtx.Col[i * cntInRow + k] > mtx.Col[i * cntInRow + k + 1]) {
                    tmp = mtx.Col[i * cntInRow + k];
                    mtx.Col[i * cntInRow + k] = mtx.Col[i * cntInRow + k + 1];
                    mtx.Col[i * cntInRow + k + 1] = tmp;
                }
    }

    for (i = 0; i < cntInRow * N; i++)
        mtx.Value[i] = next() * MAX_VAL;

    c = 0;
    for (i = 0; i <= N; i++) {
        mtx.RowIndex[i] = c;
        c += cntInRow;
    }
}

void GenerateSpecialCRS(int seed, int N, int cntInRow, crsMatrix &mtx) {
    srand(seed);
    double end = pow((double) cntInRow, 1.0 / 3.0);
    double step = end / N;

    std::vector<int> *columns = new std::vector<int>[N];
    int NZ = 0;

    for (int i = 0; i < N; i++) {
        int rowNZ = int(pow((double(i + 1) * step), 3) + 1);
        NZ += rowNZ;
        int num1 = (rowNZ - 1) / 2;
        int num2 = rowNZ - 1 - num1;

        if (rowNZ != 0) {
            if (i < num1) {
                num2 += num1 - i;
                num1 = i;
                for (int j = 0; j < i; j++)
                    columns[i].push_back(j);
                columns[i].push_back(i);
                for (int j = 0; j < num2; j++)
                    columns[i].push_back(i + 1 + j);
            } else {
                if (N - i - 1 < num2) {
                    num1 += num2 - (N - 1 - i);
                    num2 = N - i - 1;
                }
                for (int j = 0; j < num1; j++)
                    columns[i].push_back(i - num1 + j);
                columns[i].push_back(i);
                for (int j = 0; j < num2; j++)
                    columns[i].push_back(i + j + 1);
            }
        }
    }

    AllocateCRSMatrix(N, NZ, mtx);

    int count = 0;
    int sum = 0;
    for (int i = 0; i < N; i++) {
        mtx.RowIndex[i] = sum;
        sum += columns[i].size();
        for (unsigned int j = 0; j < columns[i].size(); j++) {
            mtx.Col[count] = columns[i][j];
            mtx.Value[count] = next();
            count++;
        }
    }
    mtx.RowIndex[N] = sum;

    delete[] columns;
}

int SeqCRSMult(crsMatrix A, double *x, double *y, double &time) {
    double begin = omp_get_wtime();

    // Loop by rows
    for (auto i = 0; i < A.N; i++) {
// Computing the i-th component of the vector y
        double sum = 0;
        auto j1 = A.RowIndex[i];
        auto j2 = A.RowIndex[i + 1];
        for (auto j = j1; j < j2; j++)
            sum = sum + A.Value[j] * x[A.Col[j]];
        y[i] = sum;
    }

    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}

int OmpCRSMult(crsMatrix A, double *x, double *y, int numThreads, double &time) {
    double begin = omp_get_wtime();
    size_t i;
    omp_set_num_threads(numThreads);
#pragma omp parallel for shared(A, x, y) private(i) default(none)
    // Loop by rows
    for (i = 0; i < A.N; i++) {
// Computing the i-th component of the vector y
        double sum = 0;
        auto j1 = A.RowIndex[i];
        auto j2 = A.RowIndex[i + 1];
        for (auto j = j1; j < j2; j++)
            sum = sum + A.Value[j] * x[A.Col[j]];
        y[i] = sum;
    }

    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}

int OclRegMult(regMatrix A, double *x, double *b, double &time) {
    double begin = omp_get_wtime();

    auto d = getOclDevice(false);
    auto kernel = read_path("OclRegMult.cl");

    std::vector<std::string> programStrings;
    programStrings.push_back(kernel);
    cl::Program program(programStrings);
    try {
        program.build();
    } catch (...) {
        // Print build info for all devices
        cl_int buildErr = CL_SUCCESS;
        auto buildInfo = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&buildErr);
        for (auto &pair: buildInfo) {
            std::cerr << pair.second << std::endl << std::endl;
        }
        return 1;
    }

    cl::Buffer aBuffer(A.Value, A.Value + A.N * A.N, true);
    cl::Buffer xBuffer(x, x + A.N, true);
    cl::Buffer bBuffer(b, b + A.N, false);

    auto program2Kernel =
            cl::KernelFunctor<cl::Buffer, cl::Buffer, cl::Buffer>(program, "OclRegMult");
    program2Kernel(
            cl::EnqueueArgs(
                    cl::NDRange(A.N)),
            aBuffer,
            xBuffer,
            bBuffer);
    cl::copy(bBuffer, b, b + A.N);

    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}

int OclCSRMult(crsMatrix A, double *x, double *b, double &time) {
    double begin = omp_get_wtime();

    auto d = getOclDevice(false);
    auto kernel = read_path("OclRegMult.cl");

    std::vector<std::string> programStrings;
    programStrings.push_back(kernel);
    cl::Program program(programStrings);
    try {
        program.build();
    } catch (...) {
        // Print build info for all devices
        cl_int buildErr = CL_SUCCESS;
        auto buildInfo = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&buildErr);
        for (auto &pair: buildInfo) {
            std::cerr << pair.second << std::endl << std::endl;
        }
        return 1;
    }

    cl::Buffer vBuffer(A.Value, A.Value + A.N * A.N, true);
    cl::Buffer cBuffer(A.Col, A.Col + A.N * A.N, true);
    cl::Buffer rBuffer(A.RowIndex, A.RowIndex + A.N * A.N, true);
    cl::Buffer xBuffer(x, x + A.N, true);
    cl::Buffer bBuffer(b, b + A.N, false);

    auto program2Kernel =
            cl::KernelFunctor<cl::Buffer, cl::Buffer, cl::Buffer, cl::Buffer, cl::Buffer>(program, "OclCRSMult");
    program2Kernel(
            cl::EnqueueArgs(
                    cl::NDRange(A.N)),
            vBuffer,
            cBuffer,
            rBuffer,
            xBuffer,
            bBuffer);
    cl::copy(bBuffer, b, b + A.N);

    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}

int OmpRegMult(regMatrix A, double *x, double *b, int numThreads, double &time) {
    double begin = omp_get_wtime();

    size_t i;
    omp_set_num_threads(numThreads);
#pragma omp parallel for shared(A, x, b) private(i)
    for (i = 0; i < A.N; ++i) {
        b[i] = 0;
        for (auto j = 0; j < A.N; ++j) {
            b[i] += A.Value[A.N * i + j] * x[j];
        }
    }
    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}

int SeqRegMult(regMatrix A, double *x, double *b, double &time) {
    double begin = omp_get_wtime();

    for (auto i = 0; i < A.N; ++i) {
        b[i] = 0;
        for (auto j = 0; j < A.N; ++j) {
            b[i] += A.Value[A.N * i + j] * x[j];
        }
    }

    double end = omp_get_wtime();
    time = (double) (end - begin);
    return 0;
}


int CRStoReg(crsMatrix iM, regMatrix *oM) {
    for (auto i = 0; i < iM.N; ++i) {
        for (auto j = 0; j < iM.N; ++j) {
            oM->Value[i * oM->N + j] = 0;
        }
    }

    for (auto i = 0; i < iM.N; ++i) {
        for (auto k = iM.RowIndex[i]; k < iM.RowIndex[i + 1]; ++k) {
            auto j = iM.Col[k];
            auto x = iM.Value[k];
            oM->Value[oM->N * i + j] = x;
        }
    }

    return 0;
}

int CompareVectors(double *vec1, double *vec2, int n, double &diff) {
    diff = 0.0;
    for (int i = 0; i < n; i++) {
        if (fabs(vec1[i] - vec2[i]) > EPS) {
            diff = fabs(vec1[i] - vec2[i]);
            return 1;
        }
    }
    return 0;
}

int CompareMatrices(regMatrix &A, regMatrix &B) {
    for (auto i = 0; i < A.N; ++i) {
        for (auto j = 0; j < A.N; ++j) {
            if (fabs(A.Value[i * A.N + j] - B.Value[i * B.N + j]) >= EPS) {
                return 1;
            }
        }
    }
    return 0;
}


int SeqRegMult(regMatrix A, regMatrix B, regMatrix &C, double &time) {
    double start = omp_get_wtime();

    if (A.N != B.N) return 1;
    if (A.N != C.N) return 1;

    for (auto i = 0; i < A.N; ++i) {
        for (auto j = 0; j < A.N; ++j) {
            double s = 0;
            for (auto k = 0; k < A.N; ++k) {
                s += A.Value[i * A.N + k] * B.Value[k * B.N + j];
            }
            C.Value[i * C.N + j] = s;
        }
    }

    double finish = omp_get_wtime();
    time = (double) (finish - start);

    return 0;
}

int OmpRegMult(regMatrix A, regMatrix B, regMatrix &C, int numThreads, double &time) {
    double start = omp_get_wtime();

    if (A.N != B.N) return 1;
    if (A.N != C.N) return 1;

    size_t i;
    size_t j;

    omp_set_num_threads(numThreads);
#pragma omp parallel for shared(A, B, C, numThreads) private (i, j) default(none)
    for (i = 0; i < A.N; ++i) {
        for (j = 0; j < A.N; ++j) {
            double s = 0;
            for (auto k = 0; k < A.N; ++k) {
                s += A.Value[i * A.N + k] * B.Value[k * B.N + j];
            }
            C.Value[i * C.N + j] = s;
        }

    }

    double finish = omp_get_wtime();
    time = (double) (finish - start);

    return 0;
}

double Transpose2(crsMatrix imtx, crsMatrix &omtx) {
    double start, finish;
    int i, j;

    start = omp_get_wtime();

    AllocateCRSMatrix(imtx.N, imtx.NZ, omtx);

    memset(omtx.RowIndex, 0, (imtx.N + 1) * sizeof(int));
    for (i = 0; i < imtx.NZ; i++)
        omtx.RowIndex[imtx.Col[i] + 1]++;

    int S = 0;
    for (i = 1; i <= imtx.N; i++) {
        int tmp = omtx.RowIndex[i];
        omtx.RowIndex[i] = S;
        S = S + tmp;
    }

    for (i = 0; i < imtx.N; i++) {
        int j1 = imtx.RowIndex[i];
        int j2 = imtx.RowIndex[i + 1];
        int Col = i; // Столбец в AT - строка в А
        for (j = j1; j < j2; j++) {
            double V = imtx.Value[j];  // Значение
            int RIndex = imtx.Col[j];  // Строка в AT
            int IIndex = omtx.RowIndex[RIndex + 1];
            omtx.Value[IIndex] = V;
            omtx.Col[IIndex] = Col;
            omtx.RowIndex[RIndex + 1]++;
        }
    }

    finish = omp_get_wtime();

    return double(finish - start);
}

int SeqCRSMult(crsMatrix A, crsMatrix BB, crsMatrix &C, double &time) {
    crsMatrix B;
    AllocateCRSMatrix(A.N, A.NZ, B);
    Transpose2(BB, B);

    if (A.N != B.N)
        return 1;

    int N = A.N;
    int i, j, k;

    std::vector<int> columns;
    std::vector<double> values;
    std::vector<int> row_index;

    int NZ = 0;

    int *temp = new int[N];
    row_index.push_back(0);

    for (i = 0; i < N; i++) {
        // i-я строка матрицы A
        // Обнуляем массив указателей на элементы
        memset(temp, -1, N * sizeof(int));
        // Идем по ненулевым элементам строки и заполняем массив указателей
        int ind1 = A.RowIndex[i], ind2 = A.RowIndex[i + 1];
        for (j = ind1; j < ind2; j++) {
            int col = A.Col[j];
            temp[col] = j; // Значит, что a[i, НОМЕР] лежит
            // в ячейке массива Value с номером temp[НОМЕР]
        }
        // Построен индекс строки i матрицы A
        // Теперь необходимо умножить ее на каждую из строк матрицы BT
        for (j = 0; j < N; j++) {
            // j-я строка матрицы B
            double sum = 0;
            int ind3 = B.RowIndex[j], ind4 = B.RowIndex[j + 1];
            // Все ненулевые элементы строки j матрицы B
            for (k = ind3; k < ind4; k++) {
                int bcol = B.Col[k];
                int aind = temp[bcol];
                if (aind != -1)
                    sum += A.Value[aind] * B.Value[k];
            }
            if (fabs(sum) > EPS) {
                columns.push_back(j);
                values.push_back(sum);
                NZ++;
            }
        }
        row_index.push_back(NZ);
    }

    AllocateCRSMatrix(N, NZ, C);

    for (j = 0; j < NZ; j++) {
        C.Col[j] = columns[j];
        C.Value[j] = values[j];
    }
    for (i = 0; i <= N; i++)
        C.RowIndex[i] = row_index[i];

    delete[] temp;

    FreeCRSMatrix(B);

    return 0;
}

int OmpCRSMult(crsMatrix A, crsMatrix B, crsMatrix &C, int numThreads, double &time) {
    double start = omp_get_wtime();

    if (A.N != B.N)
        return 1;

    int N = A.N;
    int i, j, k;

    std::vector<int> columns;
    std::vector<double> values;
    std::vector<int> row_index;

    int NZ = 0;

    row_index.push_back(0);
    omp_set_num_threads(numThreads);
#pragma omp parallel shared(A, B, C, numThreads, N, columns, values, NZ, row_index) private(i, j, k) default(none)
#pragma omp for ordered
    for (i = 0; i < N; i++) {
        int *temp = new int[N];
        // i-я строка матрицы A
        // Обнуляем массив указателей на элементы
        memset(temp, -1, N * sizeof(int));
        // Идем по ненулевым элементам строки и заполняем массив указателей
        int ind1 = A.RowIndex[i], ind2 = A.RowIndex[i + 1];
        for (j = ind1; j < ind2; j++) {
            int col = A.Col[j];
            temp[col] = j; // Значит, что a[i, НОМЕР] лежит
            // в ячейке массива Value с номером temp[НОМЕР]
        }
        // Построен индекс строки i матрицы A
        // Теперь необходимо умножить ее на каждую из строк матрицы BT
        for (j = 0; j < N; j++) {
            // j-я строка матрицы B
            double sum = 0;
            int ind3 = B.RowIndex[j], ind4 = B.RowIndex[j + 1];
            // Все ненулевые элементы строки j матрицы B
            for (k = ind3; k < ind4; k++) {
                int bcol = B.Col[k];
                int aind = temp[bcol];
                if (aind != -1)
                    sum += A.Value[aind] * B.Value[k];
            }
            if (fabs(sum) > EPS) {
#pragma omp critical (FILEWRITE)
                {
                    columns.push_back(j);
                    values.push_back(sum);
                    NZ++;
                }
            }
        }
        row_index.push_back(NZ);
        delete[] temp;
    }

    AllocateCRSMatrix(N, NZ, C);

#pragma omp parallel shared(C, N, NZ, columns, values, row_index) private(i, j) default(none)
    {
#pragma omp for
        for (j = 0; j < NZ; j++) {
            C.Col[j] = columns[j];
            C.Value[j] = values[j];
        }
#pragma omp for
        for (i = 0; i <= N; i++)
            C.RowIndex[i] = row_index[i];
    }

    double finish = omp_get_wtime();
    time = (double) (finish - start);

    return 0;
}